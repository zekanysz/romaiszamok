﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RomanNumberCalculator
{
    class Calculate
    {

        // Maps letters to numbers.
        private Dictionary<char, int> CharValues = null;
        public void CalculateNumber()
        {
            Console.WriteLine("Please write the first Roman number:");
            string firstNumber = Console.ReadLine();

            Console.WriteLine("Please add the operation (+, -):");
            string operation = Console.ReadLine();

            Console.WriteLine("please write the second roman number:");
            string secondnumber = Console.ReadLine();

            int fN = RomanToArabic(firstNumber);
            int sN = RomanToArabic(secondnumber);

            char t = Convert.ToChar(operation);
            int res = Calculation(fN, sN, t);

            string final = ArabicToRoman(res);

            Console.WriteLine(fN + " " + operation + " " + sN);

            Console.WriteLine(res);
            Console.WriteLine(final);
        }


        private int Calculation(int fNumber, int sNumber, char operation)
        {

            int result = 0;

            if (operation == '+')
            {
                result = fNumber + sNumber;
            }
            else if (operation == '-')
            {
                result = fNumber - sNumber;
            }

            return result;
        }




        // Convert Roman numerals to an integer.
        public int RomanToArabic(string roman)
        {
            if (CharValues == null)
            {
                CharValues = new Dictionary<char, int>();
                CharValues.Add('I', 1);
                CharValues.Add('V', 5);
                CharValues.Add('X', 10);
                CharValues.Add('L', 50);
                CharValues.Add('C', 100);
                CharValues.Add('D', 500);
                CharValues.Add('M', 1000);
            }

            if (roman.Length == 0)
                return 0;

            roman = roman.ToUpper();

            int total = 0;
            int last_value = 0;
            for (int i = roman.Length - 1; i >= 0; i--)
            {
                int new_value = CharValues[roman[i]];

                // See if we should add or subtract.
                if (new_value < last_value)
                    total -= new_value;
                else
                {
                    total += new_value;
                    last_value = new_value;
                }
            }

            // Return the result.
            return total;
        }


        private string[] ThouLetters = { "", "M", "MM", "MMM", "MMMM" };
        private string[] HundLetters =
            { "", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM" };
        private string[] TensLetters =
            { "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC" };
        private string[] OnesLetters =
            { "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX" };
        // Convert Roman numerals to an integer.
        private string ArabicToRoman(int arabic)
        {
            // See if it's >= 4000.
            if (arabic >= 4000)
            {
                // Use parentheses.
                int thou = arabic / 1000;
                arabic %= 1000;
                return "(" + ArabicToRoman(thou) + ")" +
                    ArabicToRoman(arabic);
            }

            // Otherwise process the letters.
            string result = "";

            // Pull out thousands.
            int num;
            num = arabic / 1000;
            result += ThouLetters[num];
            arabic %= 1000;

            // Handle hundreds.
            num = arabic / 100;
            result += HundLetters[num];
            arabic %= 100;

            // Handle tens.
            num = arabic / 10;
            result += TensLetters[num];
            arabic %= 10;

            // Handle ones.
            result += OnesLetters[arabic];

            return result;
        }

    }
}
